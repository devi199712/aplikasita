var assets = './../../assets/';

var ImageSource = {
    account: {
        logo: require(assets + 'logo.png'),
        logoland: require(assets + 'logo_land.png'),
        logofull: require(assets + 'logo_full.png'),
        landing1 : require(assets + 'landing1.png'),
        landing2 : require(assets + 'landing2.png'),
        landing3 : require(assets + 'landing3.png'),
    }
};

export default ImageSource;