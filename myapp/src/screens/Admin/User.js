import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet, FlatList,  ScrollView } from 'react-native'
import GlobalConfig from "../../component/config/GlobalConfig";
import Loader from "../../component/loader/loader";
import AntDesign from "react-native-vector-icons/AntDesign";
import {Menu, MenuOption, MenuOptions, MenuTrigger} from 'react-native-popup-menu';
import Entypo from 'react-native-vector-icons/Entypo';
import CustomHeader from "../../component/header/CustomHeader";
import {
    Button,
    Container, Icon, Fab
} from "native-base";

var that;

class ListUser extends React.PureComponent {
    render() {
        return (
            <View style={{borderBottomWidth:1, flexDirection:'row', 
          paddingHorizontal:5,paddingVertical:5,borderBottomColor:'rgba(255,255,255,0.3)'}}>
              <View style={{width:'95%'}}>
                <Text style={{color:'white'}}>{this.props.data.nama}</Text>
                <Text style={{color:'white'}}>{this.props.data.email}</Text>
              </View>
                <View style={{width:'10%'}}>
                    <Menu>
                        <MenuTrigger triggerTouchable={{activeOpacity: 1}}>
                            <Entypo
                                name={'dots-three-vertical'}
                                size={16}
                                color={'white'}
                            />
                        </MenuTrigger>
                        <MenuOptions style={{backgroundColor:'white'}}>
                            <MenuOption onSelect={()=>that.delete(this.props.data.id)} style={{flexDirection:'row',
                            backgroundColor: '#0D47A1'}}>
                                <View>
                                    <AntDesign
                                        name={'delete'}
                                        size={16}
                                        color={'white'}
                                    />
                                </View>
                                <Text style={{color:'white'}}>Hapus</Text>
                            </MenuOption>
                        </MenuOptions>
                    </Menu>
                </View>
            </View>
        );
    }
}

export default class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listUser: [],
        };
    }

   componentDidMount() {
        this._onFocusListener = this.props.navigation.addListener(
            "didFocus",
            payload => {
                this.loadUser()
            }
        );
    }

   static navigationOptions = {
       header: null
    };


    loadUser(){
      this.setState({
            loading: true,
        });
        var url = GlobalConfig.URL_SERVER + 'getAllUser';
        
        fetch(url, {
            headers: {
                
            },
            method: 'GET',
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        loading: false,
                        listUser: response.data
                    });

                } else {
                    this.setState({
                        loading: false,
                    });
                  }
                    
            })
            .catch(error => {
                this.setState({
                    loading: false,
                });
                setTimeout(() => alert('cek koneksi anda'), 312);
            });
    }

    delete(id){
         this.setState({
            loading: true,
        });
        var url = GlobalConfig.URL_SERVER + 'deleteUser';
        var formData = new FormData();
        formData.append('id', id);
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        loading: false,
                    });
                    alert('User Berhasil Dihapus')
                     this.loadUser()
                } else {
                    this.setState({
                        loading: false,
                    });
                    
                     alert('gagal menyimpan data')
                  }
                    
            })
            .catch(error => {
                this.setState({
                    loading: false,
                });
                setTimeout(() => alert('cek koneksi anda'), 312);
            });
    }

    navigateMenu(uri){
      this.props.navigation.navigate(uri)
    }

  _renderUser = ({ item, index }) => <ListUser data={item} index={index} />;

   render() {
    that = this;
      return (
         <Container style={styles.container}>
         <Loader loading={this.state.loading}/>
         <CustomHeader navigation={this.props.navigation} title={'User'} left={true}/>
         <ScrollView>

            <FlatList
                        data={this.state.listUser}
                        renderItem={this._renderUser}
                        keyExtractor={(item, index) => index.toString()}
                    />
            </ScrollView>
            <Fab
                    active={false}
                    direction="up"
                    containerStyle={{}}
                    style={{
                        backgroundColor: '#0D47A1',
                    }}
                    position="bottomRight"
                    onPress={() => this.navigateMenu('UserForm')}>
                    <AntDesign
                        name="plus"
                        style={{
                            fontSize: 20,
                            fontWeight: 'bold',
                        }}
                    />
                </Fab>
         </Container>
      )
   }
}


const styles = StyleSheet.create({
   container: {
      backgroundColor: '#1E88E5',
      flex:1
   },
});