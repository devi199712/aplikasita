export default {
  white_1st: '#FFFFFF',
  white_2st: '#F6F6F6',

  green_1st: '#47C363',
  green_2st: '#AAFC9A',
  gray_1st: '#E8E8E8',
  gray_2st: '#CCCCCC',
  gray_3st: '#A0A0A0',
  gray_4st: '#C4C4C4',

  blue_1st: '#33A0E8',
  blue_2st: '#3D4C5F',
  blue_3st: '#3152ff',
  red_1st: '#F86164',
  black_1st: '#000000',
  orange_1st: '#FEB031',

  purple_1st: '#4C56C0',
  purple_2st: '#C9CBEC',
  purple_3st: '#272A60'
};
