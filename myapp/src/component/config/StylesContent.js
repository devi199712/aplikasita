import {Dimensions, StyleSheet} from 'react-native';
import ColorsSource from './ColorsSource';
var { width, height } = Dimensions.get('window');
import {RFValue} from 'react-native-responsive-fontsize';

const StylesContent = StyleSheet.create({
    w04: {width: '4%'},
    w10: {width: '10%'},
    w20: {width: '20%'},
    w25: {width: '25%'},
    w30: {width: '30%'},
    w35: {width: '35%'},
    w40: {width: '40%'},
    w50: {width: '50%'},
    w60: {width: '60%'},
    w70: {width: '70%'},
    w80: {width: '80%'},
    w90: {width: '90%'},
    w100: {width: '100%'},
    wh15: {width: RFValue(15), height: RFValue(15)},
    wh20: {width: RFValue(20), height: RFValue(20)},
    wh25: {width: RFValue(25), height: RFValue(25)},
    wh30: {width: RFValue(30), height: RFValue(30)},
    wh40: {width: RFValue(40), height: RFValue(40)},

    wh30cc: {
        width: RFValue(30),
        height: RFValue(30),
        justifyContent: 'center',
        alignItems: 'center',
    },
    marginTop5:{
        marginTop: RFValue(5)
    },
    marginTop10:{
        marginTop: RFValue(10)
    },
    marginTop20:{
        marginTop: RFValue(20)
    },
    marginBottom5:{
        marginBottom: RFValue(5)
    },
    marginBottom20:{
        marginBottom: RFValue(20)
    },

    wh100: {width: '100%', height: '100%'},

    flexR: {flexDirection: 'row'},
    flexC: {flex: 1, flexDirection: 'column'},

    centerPage: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    centerPage_fDr: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    centerPage_fDc: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    centerPage_0000: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },

    containerFlex: {
        flex: 1,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: ColorsSource.white_1st,
    },
    containerPurple: {
        flex: 1,
        backgroundColor: ColorsSource.purple_1st,
    },
    content_pH15: {
        width: '100%',
        paddingHorizontal: RFValue(15),
        justifyContent: 'center',
        alignItems: 'center',
    },
    content_pH20: {
        width: '100%',
        paddingHorizontal: RFValue(20),
        justifyContent: 'center',
        alignItems: 'center',
    },
    content_w20: {
        width: '20%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    pV20_pH15: {
        paddingVertical: RFValue(20),
        paddingHorizontal: RFValue(15),
    },

    contentBtn:{
        paddingHorizontal:RFValue(20),
        paddingBottom:RFValue(15)
    },

    //BUTTON

    btnPurple: {
        height: RFValue(35),
        backgroundColor: ColorsSource.purple_1st,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: RFValue(5),
    },

    btnWhite: {
        width: '100%',
        height: RFValue(35),
        backgroundColor: ColorsSource.white_1st,
        borderRadius: RFValue(5),
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },

    btnFooter: {
        height: RFValue(40),
        backgroundColor: ColorsSource.green_1st,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: RFValue(5),
        marginTop: RFValue(20),
        marginBottom: RFValue(10),
        marginHorizontal: RFValue(10),
    },

    btn100_cg1_bR5_pV2_pH5: {
        paddingVertical: RFValue(2),
        paddingHorizontal: RFValue(5),
        backgroundColor: ColorsSource.green_1st,
        borderRadius: RFValue(5),
        justifyContent: 'center',
        alignItems: 'center',
    },
    btn100_cg1_bR5_pV7_pH7: {
        paddingVertical: RFValue(7),
        paddingHorizontal: RFValue(7),
        backgroundColor: ColorsSource.green_1st,
        borderRadius: RFValue(5),
        justifyContent: 'center',
        alignItems: 'center',
    },
    btn48_cr1_bR5_pV5: {
        width: '48%',
        backgroundColor: ColorsSource.red_1st,
        borderRadius: RFValue(5),
        paddingVertical: RFValue(5),
    },
    btn48_cg1_bR5_pV5: {
        width: '48%',
        backgroundColor: ColorsSource.green_1st,
        borderRadius: RFValue(5),
        paddingVertical: RFValue(5),
    },
});

export default StylesContent;