import React, { Component } from 'react'
import { View, Text, TouchableOpacity, AsyncStorage, TextInput, StyleSheet, FlatList,  ScrollView } from 'react-native'
import GlobalConfig from "../../component/config/GlobalConfig";
import Loader from "../../component/loader/loader";
import AntDesign from "react-native-vector-icons/AntDesign";
import {Menu, MenuOption, MenuOptions, MenuTrigger} from 'react-native-popup-menu';
import Entypo from 'react-native-vector-icons/Entypo';
import Plotly from 'react-native-plotly';
import CustomHeader from "../../component/header/CustomHeader";

import {
    Button,
    Container, Icon, Fab
} from "native-base";

var that;

export default class DetailProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

   componentDidMount() {
    AsyncStorage.getItem("idProduct").then(idProduct => {
        this.setState({
            idProduct: JSON.parse(idProduct),
        })
    })
      this._onFocusListener = this.props.navigation.addListener(
          "didFocus",
          payload => {
              this.loadDetail()
          }
      );
    }

         static navigationOptions = {
        header: null
    };



    loadDetail(){
      this.setState({
            loading: true,
        });
        var url = GlobalConfig.URL_SERVER + 'getMac';
        var formData = new FormData();
        formData.append('mac', this.state.idProduct);
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        loading: false,
                        dataProduct: response.formula,
                        x: response.formula.x,
                        y: response.formula.y,
                        x2: response.real.x,
                        y2: response.real.y,
                    });
                    this.formulaError()
            } else {
                    this.setState({
                      loading: false,
                    });
                  }
                    
            })
            .catch(error => {
                this.setState({
                    loading: false,
                });
                setTimeout(() => alert('cek koneksi anda'), 312);
            });
    }

    formulaError(){
      let xe = parseFloat(this.state.x)
      let ye = parseFloat(this.state.y)
      let xr = parseFloat(this.state.x2)
      let yr =  parseFloat(this.state.y2)

      let f1 = (xe - xr) * (xe - xr)
      let f2 = (ye - yr) * (ye - yr)
      let f3 = f1 + f2
      let f4 = Math.sqrt(parseFloat(f3))
      this.setState({
        valueError : f4.toFixed(4)
      })
    }

   render() {
    that = this;
      return (
        <Container style={styles.container}>
           <Loader loading={this.state.loading}/>
           <CustomHeader navigation={this.props.navigation} title={'Detail Product'} left={true}/>
          <View style={styles.content}>
          <Text>Nilai Error {this.state.valueError}</Text>
            <View style={styles.chartRow}>
              <Plotly
                data={
                  [{
                      x: [this.state.x],
                      y: [this.state.y],
                      type: 'scatter',
                      mode: 'markers',
                      marker: {color: 'red'},
                      name: 'pengujian',
                      showlegend: true,
                    },
                    {
                      x: [this.state.x2],
                      y: [this.state.y2],
                      type: 'scatter',
                      mode: 'markers',
                      marker: {color: 'green'},
                      name: 'real',
                      showlegend: true,
                    },
                  ]}  
              />
            </View>
          </View>
        </Container>
      )
   }
}


const styles = StyleSheet.create({
  chartRow: {
    flex: 1,
    width: '100%',
  },
  content: {
    width: '100%',
    height: '90%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});