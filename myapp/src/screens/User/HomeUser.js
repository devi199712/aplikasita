import React, { Component } from 'react'
import { View, Text, TouchableOpacity, AsyncStorage, TextInput, StyleSheet, FlatList,  ScrollView } from 'react-native'
import GlobalConfig from "../../component/config/GlobalConfig";
import Loader from "../../component/loader/loader";
import AntDesign from "react-native-vector-icons/AntDesign";
import {Menu, MenuOption, MenuOptions, MenuTrigger} from 'react-native-popup-menu';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import CustomHeader from "../../component/header/CustomHeader";
import {
    Button,
    Container, Icon, Fab
} from "native-base";

var that;


class ListProduct extends React.PureComponent {

  detail(id){
      AsyncStorage.setItem('idProduct', JSON.stringify(id)).then(() => {
         that.props.navigation.navigate('DetailProduct');
      })
  }
  
  render() {
      return (
            <TouchableOpacity style={[styles.card]} onPress={()=>this.detail(this.props.data.mac)}>
                <View style={{width:'90%'}}>
                    <Text style={{color:'white'}}>{this.props.data.name}</Text>
                </View>
                <View style={{width:'10%', justifyContent: 'center', alignItems: 'center'}}>
                    <Entypo
                        name={'chevron-small-right'}
                        size={25}
                        color={'white'}
                    />
                </View>
            </TouchableOpacity>
        );
    }
}

export default class HomeUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listProduct: [],
        };
    }


    componentDidMount() {
        this._onFocusListener = this.props.navigation.addListener(
            "didFocus",
            payload => {
                this.loadProduct()
            }
        );
    }

    static navigationOptions = {
        header: null
    };


    loadProduct(){
      this.setState({
            loading: true,
        });
        var url = GlobalConfig.URL_SERVER + 'getProduct';
        
        fetch(url, {
            headers: {
                
            },
            method: 'GET',
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        loading: false,
                        listProduct: response.data
                    });

                } else {
                    this.setState({
                        loading: false,
                    });
                  }
                    
            })
            .catch(error => {
                this.setState({
                    loading: false,
                });
                setTimeout(() => alert('cek koneksi anda'), 312);
            });
    }

  
  _renderProduct = ({ item, index }) => <ListProduct data={item} index={index} />;

   render() {
    that = this;
      return (
         <Container style={styles.container}>
           <Loader loading={this.state.loading}/>
           <CustomHeader navigation={this.props.navigation} title={'Home'} left={true}/>
           <ScrollView>
              <FlatList
                  data={this.state.listProduct}
                  renderItem={this._renderProduct}
                  keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
         </Container>
      )
   }
}


const styles = StyleSheet.create({
   container: {
      flex:1,
      backgroundColor: '#1E88E5',
   },
   input: {
      margin: 15,
      height: 40,
      borderColor: '#7a42f4',
      borderWidth: 1
   },
   card:{
      width: '100%',
      flexDirection: 'row',
      paddingHorizontal:10,
      paddingVertical:15,
      borderBottomWidth:1,
      borderBottomColor:'rgba(255,255,255,0.3)'
   }
});