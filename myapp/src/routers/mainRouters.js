import React from 'react';
import {
    createAppContainer,
    createSwitchNavigator,
    createBottomTabNavigator,
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SplashScreen from '../screens/auth/SplashScreen';
import Login from "../screens/Account/Login";
import HomeAdmin from "../screens/Admin/HomeAdmin";
import HomeUser from "../screens/User/HomeUser";
import User from "../screens/Admin/User";
import UserForm from "../screens/Admin/UserForm";
import Product from "../screens/Admin/Product";
import ProductForm from "../screens/Admin/ProductForm";
import DetailProduct from "../screens/User/DetailProduct";

export const All = createStackNavigator({
    SplashScreen: {
        screen: SplashScreen
    },
    Login: {
    	screen: Login
    },
    HomeAdmin:{
        screen: HomeAdmin
    },
    HomeUser:{
        screen: HomeUser
    },
    User:{
        screen: User
    },
    UserForm:{
        screen: UserForm
    },
    DetailProduct:{
        screen: DetailProduct
    },
    Product:{
        screen: Product
    },
    ProductForm:{
        screen: ProductForm
    }
});

export const Root = createAppContainer(All)