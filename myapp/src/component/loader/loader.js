import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    Modal,
    Image,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';

const Loader = props => {
    const {
        loading,
        title,
        ...attributes
    } = props;


    this.state = {
        isVisible: false
    }

    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={loading ? loading : this.state.isVisible}
            onRequestClose={() => { console.log('close modal') }}
        >
            <View style={styles.modalBackground}>
                <View style={styles.border}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <ActivityIndicator size="large" color="#330066" animating />  
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000090',
    },
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    border: {
        width: 80,
        height: 60,
        backgroundColor: 'gray',
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 1,
    }
});
export default Loader;
