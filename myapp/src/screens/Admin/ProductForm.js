import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet } from 'react-native'
import GlobalConfig from "../../component/config/GlobalConfig";
import Loader from "../../component/loader/loader";
import CustomHeader from "../../component/header/CustomHeader";

class ProductForm extends Component {
   state = {
    name:'',
      mac: '',
   }

   static navigationOptions = {
       header: null
    };


   addConfirmation(){
      if(this.state.name == ''){
         alert('Masukkan Nama Product')
       }
        else if(this.state.mac == ''){
         alert('Masukkan MAC')
      } else {
         this.add()
      }
   }
   
   add(){
      this.setState({
            loading: true,
        });
        var url = GlobalConfig.URL_SERVER + 'insertProduct';
        var formData = new FormData();
        formData.append('name', this.state.name);
        formData.append('mac', this.state.mac);
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        loading: false,
                    });
                     this.props.navigation.navigate('Product')
                } else {
                    this.setState({
                        loading: false,
                    });
                    
                     alert('gagal menyimpan data')
                  }
                    
            })
            .catch(error => {
                this.setState({
                    loading: false,
                });
                setTimeout(() => alert('cek koneksi anda'), 312);
            });
   }
   render() {
      return (
         <View style = {styles.container}>
         <Loader loading={this.state.loading}/>
         <CustomHeader navigation={this.props.navigation} title={'Product Form'} left={true}/>
            <TextInput style = {{marginTop:150, height: 40,margin:15, paddingHorizontal:16, width:325,
              backgroundColor:'rgba(255,255,255,0.3)',borderRadius: 25, height : 40}}
               placeholder = "Nama Product"
               placeholderTextColor = "white"
               onChangeText={text => this.setState({name: text})}
               value={this.props.name}/>
            
            <TextInput style = {styles.input}
               placeholder = "MAC"
               placeholderTextColor = "white"
               onChangeText={text => this.setState({mac: text})}
               value={this.props.mac}/>
            <TouchableOpacity
               style = {styles.submitButton}
               onPress = {() => this.addConfirmation()}>
               <Text style = {styles.submitButtonText}> TAMBAH PRODUCT </Text>
            </TouchableOpacity>
         </View>
      )
   }
}
export default ProductForm

const styles = StyleSheet.create({
   container: {
      backgroundColor: '#1E88E5',
      flex:1,
   },
   input: {
      margin: 15,
      height: 40,
      width:325,
      backgroundColor:'rgba(255,255,255,0.3)',
      borderRadius: 25,
      height : 40,
      paddingHorizontal:16,
   },
   submitButton: {
      backgroundColor: '#0D47A1',
      padding: 10,
      width: 325,
      margin: 15,
      height: 40,
      borderRadius: 25,
   },
   submitButtonText:{
      color: 'white',
      textAlign:'center'
   },
});