console.disableYellowBox = true;
import React, { Component } from 'react';
import { Root } from './src/routers/mainRouters';
import { MenuProvider } from 'react-native-popup-menu';

export default class App extends Component {
  render() {
    return (
    	<MenuProvider>
        <Root />
        </MenuProvider>
    );
  }
}
