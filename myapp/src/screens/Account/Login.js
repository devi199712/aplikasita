import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet,   ImageBackground} from 'react-native'
import GlobalConfig from "../../component/config/GlobalConfig";
import Loader from "../../component/loader/loader";

class Login extends Component {
   state = {
      email: '',
      password: ''
   }

   static navigationOptions = {
        header: null
    };

   loginConfirmation(){
      if(this.state.email == ''){
         alert('Masukkan Email')
      } else if(this.state.password == ''){
         alert('Masukkan Password')
      } else {
         this.login()
      }
   }
   
   login(){
      this.setState({
            loading: true,
        });
        var url = GlobalConfig.URL_SERVER + 'login';
        var formData = new FormData();
        formData.append('email', this.state.email);
        formData.append('password', this.state.password);
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        loading: false,
                    });
                     if(response.data.role_id == 1){
                        this.props.navigation.navigate('HomeAdmin')
                     } else {
                        this.props.navigation.navigate('HomeUser')
                     }
                } else {
                    this.setState({
                        loading: false,
                    });
                    
                     alert('email atau password salah')
                  }
                    
            })
            .catch(error => {
                this.setState({
                    loading: false,
                });
                setTimeout(() => alert('cek koneksi anda'), 312);
            });
   }
   render() {
      return (
        <View style={styles.footer}>
         <View style = {styles.img}>
         <Loader loading={this.state.loading}/>
            <ImageBackground
              source={require('../../component/img/footer.png')}
              style={{width: '100%',height:92, backgroundColor:'#0D47A1', flex:1}}
            />

            <View style = {styles.content}>
            <TextInput style = {styles.input}
               underlineColorAndroid = 'rgba(0,0,0,0)'
               placeholder = "Email"
               placeholderTextColor = "#ffffff"
               onChangeText={text => this.setState({email: text})}
               value={this.props.email}/>
            
            <TextInput style = {styles.input}
               underlineColorAndroid = 'rgba(0,0,0,0)'
               placeholder = "Password"
               placeholderTextColor = "#ffffff"
               secureTextEntry={true}
               onChangeText={text => this.setState({password: text})}
               value={this.props.password}/>
            
            <TouchableOpacity
               style = {styles.submitButton}
               onPress = {() => this.loginConfirmation()}>
               <Text style = {styles.submitButtonText}> Login </Text>
            </TouchableOpacity>
            </View>
         </View>
         <Text style={{fontSize: 15,color: 'white',textAlign:'center'}}>Do not have an account?</Text>
         <Text style={{fontSize: 15,color: 'black',textAlign:'center'}}>Contact Administrator for Register</Text>
        </View>
      )
   }
}
export default Login

const styles = StyleSheet.create({
   content: {
      justifyContent: 'center',
      backgroundColor: '#1E88E5',
      flex:4,
      paddingHorizontal: 16,
   },
   input: {
      margin: 15,
      height: 40,
      width:300,
      backgroundColor:'rgba(255,255,255,0.3)',
      borderRadius: 25,
      paddingHorizontal: 16
   },
   submitButton: {
      backgroundColor: '#0D47A1',
      padding: 10,
      width: 300,
      margin: 15,
      height: 40,
      borderRadius: 25,
   },
   submitButtonText:{
      color: 'white',
      textAlign: 'center'
   },
   img: {
    flex:1,
   },
   footer: {
    flex:1,
    backgroundColor: '#0D47A1',
   }
});

//don't have an account? Please contact admin to register