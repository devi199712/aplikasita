import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet } from 'react-native'
import GlobalConfig from "../../component/config/GlobalConfig";
import Loader from "../../component/loader/loader";
import CustomHeader from "../../component/header/CustomHeader";

class UserForm extends Component {
   state = {
    nama:'',
      email: '',
      password: ''
   }

   static navigationOptions = {
       header: null
    };

   registerConfirmation(){
      if(this.state.nama == ''){
         alert('Masukkan Nama')
       }
        else if(this.state.email == ''){
         alert('Masukkan Email')
      } else if(this.state.password == ''){
         alert('Masukkan Password')
      } else {
         this.register()
      }
   }
   
   register(){
      this.setState({
            loading: true,
        });
        var url = GlobalConfig.URL_SERVER + 'insertUser';
        var formData = new FormData();
        formData.append('role_id', 2);
        formData.append('nama', this.state.nama);
        formData.append('email', this.state.email);
        formData.append('password', this.state.password);
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        loading: false,
                    });
                     this.props.navigation.navigate('User')
                } else {
                    this.setState({
                        loading: false,
                    });
                    
                    alert('gagal menyimpan data')
                  }
                    
            })
            .catch(error => {
                this.setState({
                    loading: false,
                });
                setTimeout(() => alert('cek koneksi anda'), 312);
            });
   }
   render() {
      return (
         <View style = {styles.container}>
         <Loader loading={this.state.loading}/>
         <CustomHeader navigation={this.props.navigation} title={'User Form'} left={true}/>
            <TextInput style = {{marginTop:150, height: 40,margin:15, paddingHorizontal:16, width:325,
              backgroundColor:'rgba(255,255,255,0.3)',borderRadius: 25, height : 40}}
               placeholder = "Nama"
               placeholderTextColor = "white"
               onChangeText={text => this.setState({nama: text})}
               value={this.props.nama}/>
            
            <TextInput style = {styles.input}
               placeholder = "Email"
               placeholderTextColor = "white"
               onChangeText={text => this.setState({email: text})}
               value={this.props.email}/>
            

            <TextInput style = {styles.input}
               placeholder = "Password"
               placeholderTextColor = "white"
               secureTextEntry={true}
               onChangeText={text => this.setState({password: text})}
               value={this.props.password}/>
            
            <TouchableOpacity
               style = {styles.submitButton}
               onPress = {() => this.registerConfirmation()}>
               <Text style = {styles.submitButtonText}> Register </Text>
            </TouchableOpacity>
         </View>
      )
   }
}
export default UserForm

const styles = StyleSheet.create({
   container: {
      backgroundColor: '#1E88E5',
      flex:1,
   },
   input: {
      margin: 15,
      height: 40,
      width:325,
      backgroundColor:'rgba(255,255,255,0.3)',
      borderRadius: 25,
      height : 40,
      paddingHorizontal:16,
   },
   submitButton: {
     backgroundColor: '#0D47A1',
      padding: 10,
      width: 325,
      margin: 15,
      height: 40,
      borderRadius: 25,
   },
   submitButtonText:{
      color: 'white',
      textAlign:'center'
   },
});