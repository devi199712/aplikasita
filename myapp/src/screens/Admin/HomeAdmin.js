import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet, FlatList,  ScrollView } from 'react-native'
import GlobalConfig from "../../component/config/GlobalConfig";
import Loader from "../../component/loader/loader";
import AntDesign from "react-native-vector-icons/AntDesign";
import {Menu, MenuOption, MenuOptions, MenuTrigger} from 'react-native-popup-menu';
import Entypo from 'react-native-vector-icons/Entypo';
import CustomHeader from "../../component/header/CustomHeader";
import {
    Button,
    Container, Icon, Fab
} from "native-base";

var that;

export default class HomeAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    static navigationOptions = {
        header: null
    };

   componentDidMount() {
        this._onFocusListener = this.props.navigation.addListener(
            "didFocus",
            payload => {
                
            }
        );
    }


  
   render() {
    that = this;
      return (
         <Container style={styles.container}>
         <Loader loading={this.state.loading}/>
         <CustomHeader navigation={this.props.navigation} title={'Home Admin'} left={true}/>

            <ScrollView style={{paddingHorizontal:15}}>

            <TouchableOpacity onPress={()=>this.props.navigation.navigate('User')} style={[styles.btn,{marginTop:150}]}>
              <Text style={styles.text}>KELOLA USER</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Product')} style={styles.btn}>
              <Text style={styles.text}>KELOLA PRODUCT</Text>
            </TouchableOpacity>
          
            </ScrollView>
          
         </Container>
      )
   }
}


const styles = StyleSheet.create({
   container: {
      backgroundColor: '#1E88E5',
      flex:1
   },
   btn:{
    marginTop: 20,
    width: '100%',
    height : 40,
    justifyContent:'center',
    alignItems : 'center',
    backgroundColor : '#0D47A1',
    borderRadius: 25,
   },
   text:{
    color: 'white'
   }
});